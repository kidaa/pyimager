# $Id: CMakeLists.txt 27477 2013-11-21 13:08:20Z loose $

lofar_add_package(ACC)            # Application Configuration & Control
lofar_add_package(AMC)            # Astronomical Measures Conversions
lofar_add_package(ApplCommon)     # Application common stuff
lofar_add_package(Blob)           # Binary Large Objects
lofar_add_package(Common)         # Common stuff
lofar_add_package(MSLofar)        # MS for LOFAR based on ICD
lofar_add_package(pyparameterset) # Python ParameterSet bindings
lofar_add_package(pytools)        # Python tools
lofar_add_package(Stream)         # Low-level support for streaming data
lofar_add_package(Tools)          # Useful tools
lofar_add_package(Transport)      # Low-level transport library
lofar_add_package(MSLofar)        # LOFAR MeasurementSet definition
lofar_add_package(LofarStMan)   # Storage Manager for the main table of a LOFAR MS
