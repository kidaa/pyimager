/****************************************************************************/
/* ZEPTOOS:zepto-info */
/*     This file is part of ZeptoOS: The Small Linux for Big Computers.
 *     See www.mcs.anl.gov/zeptoos for more information.
 */
/* ZEPTOOS:zepto-info */
/* */
/* ZEPTOOS:zepto-fillin */
/*     $Id: init.c,v 1.20 2007/06/27 15:10:45 iskra Exp $
 *     ZeptoOS_Version: 1.2
 *     ZeptoOS_Heredity: FOSS_ORIG
 *     ZeptoOS_License: GPL
 */
/* ZEPTOOS:zepto-fillin */
/* */
/* ZEPTOOS:zepto-gpl */
/*      Copyright: Argonne National Laboratory, Department of Energy,
 *                 and UChicago Argonne, LLC.  2004, 2005, 2006, 2007
 *      ZeptoOS License: GPL
 * 
 *      This software is free.  See the file ZeptoOS/misc/license.GPL
 *      for complete details on your rights to copy, modify, and use this
 *      software.
 */
/* ZEPTOOS:zepto-gpl */
/****************************************************************************/

#define _GNU_SOURCE /* For posix_memalign.  */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dlfcn.h>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <pthread.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <unistd.h>

#include <bglmemmap.h>
#include <bglpersonality.h>

#include "bgl.h"
#include "zoid.h"
#include "zoid_protocol.h"
#include "zoid_api.h"
#include "zoid_mapping.h"
#include "bglco.h"


/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
void *bglco_thread_body(void *);

void *sram;
void *lockbox;
BGL_Barrier *barrier;

pid_t child_pid; 
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/



/* Maximum size of a command or reply.  In case of "userbuf" commands or
   replies, it only applies to the non-userbuf portion.

   There are two limits, because we support two distinct buffer sizes.  This
   helps save memory, since we need a separate buffers for command and reply,
   and normally at most one of them is "large".  */
int max_buffer_size_1 = 4096;
int max_buffer_size_2 = 4096*1024 + 1024;

/* If the number of tree packets needed to send a command (each packet
   carries a max. of 240 data bytes) exceeds ack_threshold, an acknowledgement
   from the server will be required (after the first packet).

   This is set to 0, which disable acknowledgements for all but input userbuf
   functions.  Experiments have shown that the performance is best then.

   Setting it to a higher value (8 seems like a good choice, since that's the
   hardware FIFO size on BG/L) enables the acknowledgements.  This slows the
   frequent-but-short commands down, but it does considerably improve the
   fairness (CNs that are closer to the ION in the tree topology do not get
   an unfair share of the bandwidth then).
*/
static int ack_threshold = 0;

BGLPersonality personality;

int my_p2p_addr;

void *vc0, *vc1;

struct zoid_buffer* packet_buffer;
int total_proc_count;
int pset_size;
int vn_mode;
int pset_proc_count;
int pending_exit_requests;
int abnormal_msg_received;

struct CNProc* cn_procs;

int ciod_pid;

struct zoid_dispatch_entry* dispatch_entries = NULL;

pthread_key_t thread_specific_key;
static pthread_t* worker_threads;
static pthread_t ciod_thread;
pthread_mutex_t ack_queue_mutex;
pthread_mutex_t output_mutex;

lock_pair pending_exit_locks;
lock_pair tree_locks;
int *sent_signals;
int *recv_signals;

char mpi_mapping[5];
/* Pset ranks may be non-contiguous, because they are always calculated in
   the XYZ mapping, while the actual mapping could be different.  We use
   this array to make them contiguous, which is easier to deal with.  */
int pset_rank_mapping[128];
/* On the wire we still use the XYZ mapping, so we also need to be able to
   translate backwards.  */
int pset_rank_mapping_rev[128];

/* Socket connections to ciodb, taken from ciod.  */
int ciod_control_socket, ciod_streams_socket;
/* Identifies whether an interrupting kill packet has been sent by the ciod
   thread.  */
int sent_kill_packet;

sigset_t sigusr1_set;

char l1flusher[32*1024] __attribute__ ((aligned (32)));

/*
 * Suspend CIOD, but first, if possible, get the control and stream sockets
 * from it (this requires that our ciod_preload.so stub is loaded into it).
 */
static int
suspend_ciod(void)
{
    int fd;
    char buf[20];
    int n;
    int transfer_sock;
    struct sockaddr_un addr;

    ciod_control_socket = ciod_streams_socket = -1;
    sent_kill_packet = 0;

    if ((fd = open("/var/run/ciod.440.pid", O_RDONLY)) < 0)
    {
	/* No CIOD?  How was this job started then?! */
	fprintf(stderr, "Failed to find a running CIOD!\n");
	return 1;
    }

    if ((n = read(fd, buf, sizeof(buf))) < 1)
    {
	perror("read ciod pid");
	return 1;
    }

    close(fd);

    buf[n - 1] = '\0';
    if ((ciod_pid = strtol(buf, NULL, 10)) < 1)
    {
	fprintf(stderr, "Invalid ciod pid\n");
	return 1;
    }

    if (!(transfer_sock = socket(PF_UNIX, SOCK_STREAM, 0)))
    {
	perror("create unix domain socket");
	return 1;
    }
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, "/var/tmp/zoid.socket");
    if (connect(transfer_sock, (struct sockaddr*)&addr, sizeof(addr)))
    {
	fprintf(stderr, "Modified CIOD not found. "
		"Will print job's output below:\n\n");

	if (kill(ciod_pid, SIGSTOP) < 0)
	{
	    perror("suspend ciod");
	    return 1;
	}
    }
    else
    {
	struct iovec iov;
	struct msghdr msg = {0};
	int fds[2];
	char msgbuf[CMSG_SPACE(sizeof(fds))];
	struct cmsghdr *cmsg;
	char tmp;

	if (kill(ciod_pid, SIGUSR1) < 0)
	{
	    perror("suspend ciod");
	    return 1;
	}

	iov.iov_base = &tmp;
	iov.iov_len = 1;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = msgbuf;
	msg.msg_controllen = sizeof(msgbuf);

	do
	{
	    n = recvmsg(transfer_sock, &msg, 0);
	} while (n < 0 && errno == EAGAIN);

	if (n < 0)
	{
	    perror("recvmsg");
	    return 1;
	}

	for (cmsg = CMSG_FIRSTHDR(&msg); cmsg != NULL;
	     cmsg = CMSG_NXTHDR(&msg, cmsg))
	{
	    if (cmsg->cmsg_level == SOL_SOCKET &&
		cmsg->cmsg_type == SCM_RIGHTS)
	    {
		memcpy(fds, CMSG_DATA(cmsg), sizeof(fds));
		ciod_control_socket = fds[0];
		ciod_streams_socket = fds[1];
		break;
	    }
	}
    }

    close(transfer_sock);

    return 0;
}

int
obtain_ciod_credentials(void)
{
    FILE* status;
    char buf[1024];
    int euid = -1, egid = -1;
    gid_t groups[512]; /* Ids are space-separated, so there can be at most half
			  as many as the buffer size, less in practice.  */
    int ngroups = 0;
    char cwd[4097];

    sprintf(buf, "/proc/%d", ciod_pid);

    if (chdir(buf) < 0)
    {
	perror("chdir ciod proc");
	return 1;
    }

    /* Parse CIOD's /proc/pid/status to obtain uid/gid info.  */
    if (!(status = fopen("status", "r")))
    {
	perror("fopen status");
	return 1;
    }

    while (fgets(buf, sizeof(buf), status))
    {
	if (strncmp(buf, "Uid:", strlen("Uid:")) == 0)
	{
	    if (sscanf(buf, "Uid: %*d %d", &euid) != 1)
	    {
		fprintf(stderr, "Error parsing /proc/pid/status\n");
		return 1;
	    }
	}
	else if (strncmp(buf, "Gid:", strlen("Gid:")) == 0)
	{
	    if (sscanf(buf, "Gid: %*d %d", &egid) != 1)
	    {
		fprintf(stderr, "Error parsing /proc/pid/status\n");
		return 1;
	    }
	}
	else if (strncmp(buf, "Groups:", strlen("Groups:")) == 0)
	{
	    char* bptr;

	    if (buf[strlen(buf) - 1] != '\n')
	    {
		/* We provide a 1K buffer.  Apparently, it wasn't enough.
		   Too bad -- we do the easy thing and bail out.  Obviously,
		   if this ever gets triggered, the buffer size can be
		   increased, or the code made to handle arbitrarily long
		   input lines.  */
		fprintf(stderr, "Implementation limit reached\n");
		return 1;
	    }

	    for (bptr = buf + strlen("Groups:"), ngroups = 0; *bptr;)
	    {
		char* new_bptr;

		groups[ngroups] = strtol(bptr, &new_bptr, 10);
		if (new_bptr == bptr)
		    break;
		ngroups++;
		bptr = new_bptr;
	    }
	}
    }
    fclose(status);

    if (euid == -1 || egid == -1 || ngroups < 1)
    {
	fprintf(stderr, "Expected data not found in /proc/pid/status\n");
	return 1;
    }

    /* Now get the initial current directory.  */
    {
	int n;

	if ((n = readlink("cwd", cwd, sizeof(cwd) - 1)) < 0)
	{
	    perror("read ciod cwd");
	    return 1;
	}
	cwd[n] = '\0';
    }

    /* We have all the data we need.  Adjust our process to match CIOD.  */
    if (chdir(cwd) < 0)
    {
	perror("initial chdir");
	return 1;
    }
    if (setgroups(ngroups, groups) < 0)
    {
	perror("setgroups");
	return 1;
    }
    if (setegid(egid) < 0)
    {
	perror("setegid");
	return 1;
    }
    if (seteuid(euid) < 0)
    {
	perror("seteuid");
	return 1;
    }

    return 0;
}

/*
 * Called from the backend shared objects to register functions on the server
 * side.
 */
void __zoid_register_functions(int header_id,
			       struct dispatch_array* dispatch_array,
			       int array_size, void (*init_func)(int),
			       void (*fini_func)(void))
{
    struct zoid_dispatch_entry* entry;

    if (!(entry = malloc(sizeof(*entry))))
    {
	fprintf(stderr, "Not enough memory!\n");
	return;
    }

    entry->header_id = header_id;
    entry->dispatch_array = dispatch_array;
    entry->array_size = array_size;
    entry->init_func = init_func;
    entry->fini_func = fini_func;

    entry->next = dispatch_entries;
    dispatch_entries = entry;
}

/*
 * Calculates the number of processes that are actually expected in the
 * current pset.  This is non-trivial because the psets need not be fully
 * filled in, and the mapping can be influenced by the user.
 * We also fill in two arrays to help with the mapping back and forth later.
 */
int
calculate_pset_size(void)
{
    int x_origin, y_origin, z_origin;
    int x_size, y_size, z_size, t_size;
    int t;
    int pset_size, pset_rank;

    /* Get the dimensions of the pset.  */
    x_size = BGLPersonality_xPsetSize(&personality);
    y_size = BGLPersonality_yPsetSize(&personality);
    z_size = BGLPersonality_zPsetSize(&personality);
    t_size = /* vn_mode ? 2 : 1 */ 2; /* We assume vn_mode so that also the
					 t==1 entries are initialized. */

    /* Get the coordinates of the first node of a pset.  */
    x_origin = BGLPersonality_xPsetOrigin(&personality);
    y_origin = BGLPersonality_yPsetOrigin(&personality);
    z_origin = BGLPersonality_zPsetOrigin(&personality);

    /* Check which CPUs in the pset are occupied.
       Do it in the XYZT order, which is the order used by pset_rank.  */
    pset_size = 0;
    pset_rank = 0;
    for (t = 0; t < t_size; t++)
    {
	int z;
	for (z = z_origin; z < z_origin + z_size; z++)
	{
	    int y;
	    for (y = y_origin; y < y_origin + y_size; y++)
	    {
		int x;
		for (x = x_origin; x < x_origin + x_size; x++)
		{
		    unsigned rank;

		    if (__zoid_mapping_to_rank(x, y, z, t, &rank) == 0)
		    {
			pset_rank_mapping[pset_rank] = pset_size;
			pset_rank_mapping_rev[pset_size] = pset_rank;
			pset_size++;
		    }
		    else
			pset_rank_mapping[pset_rank] = -1;

		    pset_rank++;
		}
	    }
	}
    }

    return pset_size;
}

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

static int
analyze_cmdline(int argc, char* argv[])
{
    int c;

    if (argc == 2 && strcmp(argv[1], "--version") == 0)
    {
	fprintf(stderr, "zoid version 1.0"
#ifdef ZEPTO_VERSION
		" (ZeptoOS version " TOSTRING(ZEPTO_VERSION) ")"
#endif
		"\n");
	exit(0);
    }

    while ((c = getopt(argc, argv, "a:b:m:")) != -1)
    {
	switch (c)
	{
	    char* str;

	    case 'a':
		ack_threshold = strtol(optarg, &str, 10);
		if (*str || ack_threshold < 0)
		{
		    fprintf(stderr,
			    "zoid: invalid acknowledgement threshold!\n");
		    return 1;
		}
		break;

	    case 'b':
		max_buffer_size_1 = strtol(optarg, &str, 10);
		if ((*str && *str != ':') ||
		    max_buffer_size_1 < TREE_DATA_SIZE)
		{
		    fprintf(stderr, "zoid: invalid maximum buffer size!\n");
		    return 1;
		}
		if (*str)
		{
		    /* Skip ':'.  */
		    str++;
		    max_buffer_size_2 = strtol(str, &str, 10);
		    if (*str || max_buffer_size_2 < TREE_DATA_SIZE ||
			max_buffer_size_2 < max_buffer_size_1)
		    {
			fprintf(stderr, "zoid: invalid maximum buffer size!\n");
			return 1;
		    }
		}
		else
		    max_buffer_size_2 = max_buffer_size_1;
		break;

	    case 'm':
		for (str = strtok(optarg, ":"); str; str = strtok(NULL, ":"))
		    if (!dlopen(str, RTLD_LAZY | RTLD_GLOBAL))
		    {
			fprintf(stderr, "zoid: failed to open %s: %s\n", str,
				dlerror());
			return 1;
		    }
		break;

	    default:
		fprintf(stderr,
			"Usage: %s [-a <ack_threshold>] [-b <buffer_size>]\n",
			argv[0]);
		return 1;
	}
    }

    return 0;
}

int
main(int argc, char* argv[])
{
    int fd_pers, fd0, fd1, fd_mem;
    struct thread_specific_data thread_data = {-1};

    if (analyze_cmdline(argc, argv))
	return 1;

    if ((fd_pers = open("/proc/personality", O_RDONLY)) < 0)
    {
	perror("open /proc/personality");
	fprintf(stderr, "Please run me on an I/O node!\n");
	return 1;
    }
    if (read(fd_pers, &personality, sizeof(personality)) !=
	sizeof(personality))
    {
	perror("read personality");
	return 1;
    }
    close(fd_pers);

    my_p2p_addr = BGLPersonality_treeP2PAddr(&personality);

    /*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
    fd_mem = open("/dev/mem", O_RDWR);
    if(fd_mem < 0) {
	perror("open /dev/mem");
	return -1;
    }

    sram = mmap(0, BGL_MEM_SRAM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, 
		fd_mem, BGL_MEM_SRAM_PHYS);
    if(sram == MAP_FAILED) {
	perror("mmap failed");
	return -1;
    }

    lockbox = mmap(0, BGL_MEM_LOCKBOX_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED,
		   fd_mem, BGL_MEM_LOCKBOX_PHYS);
    if(lockbox == MAP_FAILED) {
	perror("mmap failed");
	return -1;
    }
    close(fd_mem);

    recv_queue = sram + 9*1024;
    init_lock_pair(&recv_queue_locks);
    init_zoid_buf_pipe(recv_queue, &recv_queue_locks);

    send_queue = sram + 9*1024 + sizeof(zoid_buf_pipe);
    init_lock_pair(&send_queue_locks);
    init_zoid_buf_pipe(send_queue, &send_queue_locks);

    high_priority_send_queue = sram + 9*1024 + 2*sizeof(zoid_buf_pipe);
    init_lock_pair(&high_priority_send_queue_locks);
    init_zoid_buf_pipe(high_priority_send_queue, &high_priority_send_queue_locks);

    ack_queue = sram + 9*1024 + 3*sizeof(zoid_buf_pipe);
    init_lock_pair(&ack_queue_locks);
    init_zoid_buf_pipe(ack_queue, &ack_queue_locks);

    sent_signals = sram + 9*1024 + 3 * sizeof(zoid_buf_pipe);
    recv_signals = sent_signals + 1;
    *sent_signals = 0;
    *recv_signals = 0;

    barrier = lockbox + BARRIER_OFFSET(10);

    init_lock_pair(&pending_exit_locks);
    init_lock_pair(&tree_locks);

    memset(l1flusher, 0, sizeof(l1flusher));

    /*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

    /* Open both virtual channels.  */
    fd0 = open("/dev/tree0", O_RDWR);
    if (fd0 < 0)
    {
	perror("open /dev/tree0");
	return 1;
    }
    fd1 = open("/dev/tree1", O_RDWR);
    if (fd1 < 0)
    {
	perror("open /dev/tree1");
	return 1;
    }
    vc0 = mmap(NULL, 1023, PROT_READ|PROT_WRITE, MAP_SHARED, fd0, 0);
    if (vc0 == MAP_FAILED)
    {
	perror("mmap vc0");
	return 1;
    }
    vc1 = mmap(NULL, 1023, PROT_READ|PROT_WRITE, MAP_SHARED, fd1, 0);
    if (vc1 == MAP_FAILED)
    {
	perror("mmap vc1");
	return 1;
    }
    close(fd0);
    close(fd1);

    max_buffer_size_1 = TREE_BUFFER_ROUNDUP(max_buffer_size_1);
    max_buffer_size_2 = TREE_BUFFER_ROUNDUP(max_buffer_size_2);

    allocater_init();

    {
	void* packet_buffer_ptr = &packet_buffer;
	if (posix_memalign(packet_buffer_ptr, 16,
			   sizeof(*packet_buffer) + TREE_PACKET_SIZE))
	{
	    perror("allocate aligned memory");
	    return 1;
	}
    }

    if (pthread_key_create(&thread_specific_key, 0))
    {
	perror("create thread key");
	return 1;
    }
    if (pthread_setspecific(thread_specific_key, &thread_data))
    {
	perror("setting thread-specific data");
	return 1;
    }
    if (pthread_mutex_init(&ack_queue_mutex, NULL))
    {
	perror("create acknowledgement queue mutex");
	return 1;
    }
    if (pthread_mutex_init(&output_mutex, NULL))
    {
	perror("create output mutex");
	return 1;
    }

    sigemptyset(&sigusr1_set);
    sigaddset(&sigusr1_set, SIGRTMIN+1);
    sigprocmask(SIG_BLOCK, &sigusr1_set, NULL);

    /* A single iteration of this loop handles a complete job, from
       initialization to termination.  */
    for (;;)
    {
	int init_recv, i;
	BGLTreePacketHardHeader hardheader;
	BGLTreeStatusRegister status;
	struct InitMsgReply init_msg_rep;

	packet_buffer->userbuf_out = NULL;
	packet_buffer->userbuf_in = NULL;
	packet_buffer->ack_sent = 0;

	pset_size = BGLPersonality_numNodesInPset(&personality);
	/* This is only a very rough estimate.  There might be fewer processes
	   if the partition is not fully occupied, or more if we are in VN
	   mode. */
	pset_proc_count = pset_size;

	/* Receive init messages from all processes.  As a side effect, obtain
	   the number of processes the job consists of.  */
	for (init_recv = 0; init_recv < pset_proc_count; init_recv++)
	{
	    int pset_cpu_rank;
	    struct InitMsg* init_msg;
	    struct timespec ts = {0, 10000000};

	    do
	    {
		status = *(volatile BGLTreeStatusRegister*)
		    (vc1 + BGL_MEM_TREE_STATUS0_OFFSET);

		/* In case ZOID is invoked even for standard, CIOD jobs,
		   we want to minimize its impact.  So to prevent busy-looping,
		   we sleep for 0.01 sec.  */
		if (status.recpktcnt == 0)
		    nanosleep(&ts, NULL);
	    } while (status.recpktcnt == 0);

	    BGLTreeFIFO_recv(vc1 + BGL_MEM_TREE_HDROUT_OFFSET,
			     vc1 + BGL_MEM_TREE_DATAOUT_OFFSET,
			     &hardheader, (BGLQuad*)packet_buffer->data);

	    if (hardheader.p2p.pclass != PACKET_CLASS_CIO ||
		!hardheader.p2p.p2p)
	    {
		fprintf(stderr,
			"Unexpected message received during init phase!\n");
		return 1;
	    }

	    init_msg = (struct InitMsg*)packet_buffer->data;

	    /* If this is the first message, read the total number of compute
	       node processes.  */
	    if (!init_recv)
	    {
		int i;

		total_proc_count = init_msg->total_proc;
#if 0
		fprintf(stderr, "Job consists of %d processes\n",
			total_proc_count);
#endif
		strcpy(mpi_mapping, init_msg->mapping);
		vn_mode = init_msg->vn_mode;

		if (__zoid_mapping_init(mpi_mapping, total_proc_count, vn_mode,
					&personality) == 0)
		    pset_proc_count = calculate_pset_size();
		else
		    abort();
#if 0
		fprintf(stderr, "Expecting %d init msgs\n", pset_proc_count);
#endif
		//cn_procs = zoid_alloc(pset_proc_count * sizeof(*cn_procs));
		cn_procs = (struct CNProc*)(sram + 10*1024);
		assert(cn_procs);

		for (i = 0; i < pset_proc_count; i++)
		{
		    cn_procs[i].buffer = NULL;
		    cn_procs[i].current_buf = NULL;
		}
	    }

	    if (init_msg->pset_cpu_rank >= pset_size * (vn_mode ? 2 : 1) ||
		pset_rank_mapping[init_msg->pset_cpu_rank] == -1)
	    {
		fprintf(stderr, "Init message has invalid rank %d\n",
			init_msg->pset_cpu_rank);
		return 1;
	    }

	    pset_cpu_rank = pset_rank_mapping[init_msg->pset_cpu_rank];
	    cn_procs[pset_cpu_rank].pid = init_msg->pid;
	    cn_procs[pset_cpu_rank].p2p_addr = init_msg->p2p_addr;
	    cn_procs[pset_cpu_rank].pset_rank = init_msg->pset_rank;
	    cn_procs[pset_cpu_rank].cpu = init_msg->cpu;
	    cn_procs[pset_cpu_rank].status = PROC_STATUS_RUNNING;
#if 0
	    fprintf(stderr, "Received init message from proc %d\n",
		    cn_procs[pset_cpu_rank].pid);
#endif
	} /* for (init_recv) */
	pending_exit_requests = pset_proc_count;
#if 0
	fprintf(stderr, "Received all msgs, suspending CIOD...\n");
#endif

	/* Ugly hack warning!

	   Here's what seems to be happening: even if a job consists of just
	   one process, all compute nodes belonging to the partition where the
	   job will run are booted.  Those nodes that don't have any processes
	   to run immediately send a REQUESTRESET message to CIOD.  What can
	   happen is that nodes with a process on them are faster than those
	   without and send the ZOID init message first.  In that case ZOID
	   suspends CIOD before CIOD receives the REQUESTRESET messages.
	   Those messages are then read by ZOID, which spits out warning
	   messages about invalid packets coming from unknown sources (jobs
	   still succeed, though).

	   The sleep below is an attempt to avoid this race condition.  It
	   gives CIOD extra time to receive the REQUESTRESET messages.  It
	   actually seems to work, believe it or not.  */
	sleep(2);

	if (suspend_ciod())
	    return 1;

	/* Send an ACK.  */

	init_msg_rep.max_buffer_size = max_buffer_size_2;
	init_msg_rep.ack_threshold = ack_threshold;
	memcpy(packet_buffer->data, &init_msg_rep, sizeof(init_msg_rep));

	BGLTreePacketHardHeader_InitGlobal(&hardheader, PACKET_CLASS_CIO, 0,
					   BGLTreeCombineOp_NONE, 0, 1);
	do
	{
	    status = *(volatile BGLTreeStatusRegister*)
		(vc1 + BGL_MEM_TREE_STATUS0_OFFSET);
	} while (status.injpktcnt > 7);
	BGLTreeFIFO_send(vc1 + BGL_MEM_TREE_HDRIN_OFFSET,
			 vc1 + BGL_MEM_TREE_DATAIN_OFFSET,
			 &hardheader, (BGLQuad*)packet_buffer->data);

	/* At this point, CIOD is suspended, so we can take over its
	   responsibilities.  First, though, we need to change user/group ID,
	   as well as current working directory (it looks like it initially
	   points to where the job was submitted from, which is convenient). */
	if (obtain_ciod_credentials())
	    return 1;

	{
	    struct zoid_dispatch_entry* entry;

	    for (entry = dispatch_entries; entry; entry = entry->next)
		if (entry->init_func)
		    entry->init_func(pset_proc_count);
	}

	worker_threads = malloc(pset_proc_count * sizeof(*worker_threads));
	assert(worker_threads);

	for (i = 0; i < pset_proc_count; i++)
	    if (pthread_create(&worker_threads[i], NULL, worker_thread_body,
			       NULL))
	    {
		perror("create worker thread");
		return 1;
	    }

	if (ciod_control_socket != -1)
	    if (pthread_create(&ciod_thread, NULL, ciod_thread_body, NULL))
	    {
		perror("create ciod thread");
		return 1;
	    }

	child_pid = fork();
	if(child_pid == 0) {
	    bglco_loop();
	    return 0;
	} else if(child_pid < 0) {
	    perror("fork failed");
	    return 1;
	}

	/* All the activities are performed by worker threads at this point.
	   We just wait for these threads to finish.  */

	waitpid(child_pid, NULL, 0);

	enter_critical_section(recv_queue->locks);
	recv_queue->first = (struct zoid_buffer*)1;
	leave_critical_section(recv_queue->locks);
	
	/* HACK */
/* 	for(i = 0; i < pset_proc_count; i++) { */
/* 	    int r = sigqueue(getpid(), SIGRTMIN+1, (union sigval)0); */
/* 	    if(r) { */
/* 		perror("sigqueue failed"); */
/* 		exit(-1); */
/* 	    } */
/* 	} */

	for(i = 0; i < pset_proc_count; i++)
	    pthread_join(worker_threads[i], NULL);

	/* Job finished -- clean up.  */

	{
	    struct zoid_dispatch_entry* entry;

	    for (entry = dispatch_entries; entry; entry = entry->next)
		if (entry->fini_func)
		    entry->fini_func();
	}

	if (ciod_control_socket != -1)
	{
	    if (!sent_kill_packet)
		pthread_cancel(ciod_thread);
	    pthread_join(ciod_thread, NULL);
	    close(ciod_control_socket);
	}
	if (ciod_streams_socket != -1)
	    close(ciod_streams_socket);

	if (abnormal_msg_received)
	{
	    fprintf(stderr, "BECAUSE OF ABNORMAL MESSAGES ABOVE, CIOD WILL BE "
		    "KILLED.\n");
	    kill(ciod_pid, SIGCONT);
	    sleep(5);
	    kill(ciod_pid, SIGTERM);
	    return 1;
	}
	else
	{
#if 0
	    fprintf(stderr, "All processes terminated normally; "
		    "attempting to resume CIOD...\n");
#endif
	    if (!sent_kill_packet)
	    {
		/* First send an ACK to the processes, so that they can
		   terminate.  */

		BGLTreePacketHardHeader_InitGlobal(&hardheader,
						   PACKET_CLASS_CIO,
						   0, BGLTreeCombineOp_NONE,
						   0, 1);
		/* We don't care what's in the packet_buffer.  */
		do
		{
		    status = *(volatile BGLTreeStatusRegister*)
			(vc0 + BGL_MEM_TREE_STATUS0_OFFSET);
		} while (status.injpktcnt > 7);
		BGLTreeFIFO_send(vc0 + BGL_MEM_TREE_HDRIN_OFFSET,
				 vc0 + BGL_MEM_TREE_DATAIN_OFFSET,
				 &hardheader, (BGLQuad*)packet_buffer->data);
	    }

	    kill(ciod_pid, SIGCONT);
	}
#if 0
	{
	    extern int poll_hist[201];
	    int i;

	    fprintf(stderr, "\n\nPoll histogram:\n");
	    for (i = 1; i < sizeof(poll_hist) / sizeof(poll_hist[0]); i++)
		if (poll_hist[i])
		    fprintf(stderr, "%d %d\n", i, poll_hist[i]);
	}
#endif

	/* One of the things we don't free are zoid message buffers.  I'm
	   simply worried the repeated freeing and allocation of these large
	   memory areas could result in memory fragmentation.  */

	cleanup_traffic();

	free(worker_threads);
	for (i = 0; i < pset_proc_count; i++)
	    if (cn_procs[i].buffer)
		__zoid_release_buffer(cn_procs[i].buffer->data);
	//free(cn_procs);

	if (seteuid(0) < 0)
	{
	    perror("seteuid");
	    return 1;
	}
	if (setegid(0) < 0)
	{
	    perror("seteuid");
	    return 1;
	}
    } /* for (;;) */

    /* The code below is unreachable and is only provided for the sake of
       completeness.  */

    free(packet_buffer);

    while (dispatch_entries)
    {
	struct zoid_dispatch_entry* entry = dispatch_entries;
	dispatch_entries = dispatch_entries->next;
	free(entry);
    }

    return 0;
}



void init_lock_pair(lock_pair *locks) {
    static int next_lock = 0;

    if (pthread_mutex_init(&(locks->pt_mutex), NULL)) {
	perror("lock_pair_init");
	exit(-1);
    }
    locks->hw_mutex = lockbox + MUTEX_OFFSET(next_lock++);
    BGL_Mutex_Release(locks->hw_mutex);
}



inline void enter_critical_section(lock_pair *locks) {
    if(pthread_mutex_lock(&(locks->pt_mutex)))
    	assert(0);
    BGL_Mutex_Acquire(locks->hw_mutex);
}



inline void leave_critical_section(lock_pair *locks) {
    BGL_Mutex_Release(locks->hw_mutex);
    if(pthread_mutex_unlock(&(locks->pt_mutex)))
	assert(0);
}
