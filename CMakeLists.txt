# $Id: CMakeLists.txt 27477 2013-11-21 13:08:20Z loose $

cmake_minimum_required(VERSION 2.6)

## ---------------------------------------------------------------------------
## Preamble: set compilers
## ---------------------------------------------------------------------------
include(CMake/LofarInit.cmake)

## ---------------------------------------------------------------------------
## Project characteristics
## ---------------------------------------------------------------------------
project(LOFAR)
include(LofarGeneral)

## ---------------------------------------------------------------------------
## LOFAR packages: build all, unless BUILD_PACKAGES was specified
## ---------------------------------------------------------------------------
include(LofarPackage)
if(NOT DEFINED BUILD_PACKAGES)
  lofar_add_package(LCS)
  lofar_add_package(CEP)
  lofar_add_package(RTCP)
  lofar_add_package(SAS)
  lofar_add_package(MAC)
  lofar_add_package(LCU)
  lofar_add_package(SubSystems)
else(NOT DEFINED BUILD_PACKAGES)
  separate_arguments(BUILD_PACKAGES)
  foreach(pkg ${BUILD_PACKAGES})
    lofar_add_package(${pkg} REQUIRED)
  endforeach(pkg ${BUILD_PACKAGES})
endif(NOT DEFINED BUILD_PACKAGES)

## ---------------------------------------------------------------------------
## Check if Doxygen is present; if so, generate doxygen configuration file.
## ---------------------------------------------------------------------------
include(LofarDoxygen)

## ---------------------------------------------------------------------------
## Generate configuration header file.
## ---------------------------------------------------------------------------
include(LofarConfig)
