package org.astron.basesim;

import java.util.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public interface GraphSelectionListener extends EventListener {
  public void valueChanged(GraphSelectionEvent e);
}