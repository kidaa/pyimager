package org.astron.lofarsim;

import org.astron.basesim.GraphDataHolder;
import java.awt.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class DHBeam extends GraphDataHolder {

  public DHBeam() {
    super();
    setBackground(Color.blue);
  }

  public String getClassName() { return "DH_Beam"; }
}