package org.astron.lofarsim;

import org.astron.basesim.GraphDataHolder;
import java.awt.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class DHCorr extends GraphDataHolder {

  public DHCorr() {
    super();
    setBackground(Color.green);
  }

  public String getClassName() { return "DH_Corr"; }
}