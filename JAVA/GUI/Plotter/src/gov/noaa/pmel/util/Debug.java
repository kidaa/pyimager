/*
 * $Id: Debug.java 8825 2006-06-28 14:44:59Z pompert $
 */

package gov.noaa.pmel.util
;
/**
 * For debugging only
 */
public class Debug {
  public static final boolean DEBUG = false;
  public static final boolean DRAW_TRACE = false;
  public static final boolean TAXIS = false;
  /**
   * Write debug statements related to internal sgt
   * events.
   */
  public static final boolean EVENT = false;
  /**
   * Debug for contouring
   */
  public static final boolean CONTOUR = false;
}
