-------------------------------------------------------------------------
-------------------------------------------------------------------------
--
-- Revision Control Information
--
-- Header
--
-- $Workfile:   aukfft_fftchipa.tdf  $
--
-- $Revision:   1.21  $
--
-- $Date:   22 Mar 2002 16:29:16  $
--
-- $Title       :  FFT
--
-- $Project     :  FFT
--
-- Description  :  Subdesign : aukfft_fftchipaa
--
-- Copyright 2000 (c) Altera Corporation
-- All rights reserved
--
-------------------------------------------------------------------------
-------------------------------------------------------------------------

-- **********************************************************************
--          INSERT YOUR PARAMETERS HERE
-- **********************************************************************
--
-- The following section will need to be modified to bring the reference 
-- design in line with your core specification. The code below this section
-- does not need to be altered except where the fft core instance name has changed.
--
--
-- Parameter to update 
-- ===================
-- (General)
    CONSTANT points                 = 256;
    CONSTANT floatwidth             = 4;
    CONSTANT datawidth              = 16;
    CONSTANT twiddlewidth           = 16;
    CONSTANT backward_compatible    = 0;
--    
-- (Stratix specific)
    CONSTANT stratix                = 0;
    CONSTANT rom_in_4k_mem_blocks   = 1;
--
--  Files to include
--  ================
--  Need to update the following if new instance name for fft core and also line 153
	CONSTANT sin_file				= "myfftcore_twidsin.hex";
	CONSTANT cos_file				= "myfftcore_twidcos.hex";
	INCLUDE "myfftcore.inc";
	
-- *********************************************************************
--          END OF PARAMETER SECTION   
-- *********************************************************************  


FUNCTION aukfft_ram_dp
(
    sysclk,

    read, write,

    writeaddress[addresswidth..1],
    writereal[datawidth..1],
    writeimag[datawidth..1],

    readaddress[addresswidth..1],

    fftwriteenable, 

    fftreadaddress[addresswidth..1],
    fftwriteaddress[addresswidth..1],

    fftwritedatareal[datawidth..1],
    fftwritedataimag[datawidth..1]
)
WITH
(
    datawidth,
    addresswidth
)
RETURNS
(
    readreal[datawidth..1],
    readimag[datawidth..1],
    fftreaddatareal[datawidth..1],
    fftreaddataimag[datawidth..1]
);


FUNCTION aukfft_twidrom 
(
    sysclk, 
    address[addresswidth..1]
)
RETURNS 
(
    twreal[twiddlewidth..1], 
    twimag[twiddlewidth..1]
);


FUNCTION aukfft_twidrom_4k 
(
    sysclk, 
    address[addresswidth..1]
)
RETURNS
(
    twreal[twiddlewidth..1], 
    twimag[twiddlewidth..1]
);


constant addresswidth = log2(points);

-- if radix = 4, the FFT is pure radix 4
-- if radix = 42, the FFT is mixed radix 4 and radix 2
constant radix = ( floor(log2(points) DIV 2) == ceil(log2(points) DIV 2) ) ? 4 : 42;

constant expwidth = floatwidth + 1;

-- only use 4k memory blocks in stratix and when not using backward compatible
constant use_4k_mem_blocks = rom_in_4k_mem_blocks * stratix * (1 - backward_compatible);


subdesign aukfft_fftchipa
(
    sysclk                         : INPUT;
    reset                          : INPUT;
    go                             : INPUT;
    writeaddress[addresswidth..1]  : INPUT;
    readaddress[addresswidth..1]   : INPUT;
    read                           : INPUT;
    write                          : INPUT;
    writereal[datawidth..1]        : INPUT;
    writeimag[datawidth..1]        : INPUT;
    
    readreal[datawidth..1]         : OUTPUT;
    readimag[datawidth..1]         : OUTPUT;
    exponent[expwidth..1]          : OUTPUT;
    done                           : OUTPUT;
)


VARIABLE


					  -- edit the instance name if changed from example_ffta	
    fftcore_a     :   myfftcore;
    
    rambank       :   aukfft_ram_dp 
                      WITH 
                      (
                      datawidth           = datawidth, 
                      addresswidth        = addresswidth, 
                      stratix             = stratix
                      );
    
    
    twidrom       :   aukfft_twidrom 
                      WITH 
                      (
                      addresswidth        = addresswidth, 
                      twiddlewidth        = twiddlewidth,
                      sin_file            = sin_file, 
                      cos_file            = cos_file, 
                      stratix             = stratix, 
                      backward_compatible = backward_compatible
                      );
                    
                     
    twidrom_4k    :   aukfft_twidrom_4k         -- twiddle rom using 4k memory blocks in stratix
                      WITH 
                      (
                      addresswidth        = addresswidth, 
                      twiddlewidth        = twiddlewidth,
                      sin_file            = sin_file
                      );                                 


    readdatareal[datawidth..1]              : node;
    readdataimag[datawidth..1]              : node;
    
    realtwid[twiddlewidth..1]               : node;
    imagtwid[twiddlewidth..1]               : node;
    
    rvsreadadd[addresswidth..1]             : node;

BEGIN
        
    --*********************
    --*** FFT PROCESSOR ***
    --*********************

    fftcore_a.sysclk                    = sysclk;
    fftcore_a.reset                     = reset;
    fftcore_a.go                        = go;
                                        
    done                                = fftcore_a.done;
                                        
    fftcore_a.realdatain[]              = readdatareal[];
    fftcore_a.imagdatain[]              = readdataimag[];
    fftcore_a.realtwid[]                = realtwid[];
    fftcore_a.imagtwid[]                = imagtwid[];
                                        
    exponent[]                          = fftcore_a.exponent[];
    
    
    --**************
    --*** MEMORY ***
    --**************
    
    rambank.sysclk                      = sysclk;
    rambank.read                        = read;
    rambank.write                       = write;
    rambank.writeaddress[]              = writeaddress[];
    rambank.writereal[]                 = writereal[];
    rambank.writeimag[]                 = writeimag[];
    rambank.readaddress[]               = rvsreadadd[];     --readaddress[];
    rambank.fftwriteenable              = fftcore_a.writeenable;
    rambank.fftreadaddress[]            = fftcore_a.readaddress[];
    rambank.fftwriteaddress[]           = fftcore_a.writeaddress[];
    rambank.fftwritedatareal[]          = fftcore_a.realdataout[];
    rambank.fftwritedataimag[]          = fftcore_a.imagdataout[];
    
    
    -- Create digit or digit & bit reversed read address
    IF (radix == 42) GENERATE
    
        -- mixed radix
        -- create digit and bit reversed read address
            
        FOR k IN 1 TO (floor(addresswidth DIV 2)) GENERATE
            rvsreadadd[2*k]             = readaddress[addresswidth-2*k];
            rvsreadadd[2*k+1]           = readaddress[addresswidth+1-2*k]; 
        END GENERATE;                   
                                        
        rvsreadadd[1]                   = readaddress[addresswidth];
    
    ELSE GENERATE
    
        -- pure radix 4
        -- create digit reversed read address  
      
        FOR k IN 1 TO (addresswidth DIV 2) GENERATE
            rvsreadadd[2*k-1]           = readaddress[addresswidth+1-2*k];
            rvsreadadd[2*k]             = readaddress[addresswidth+2-2*k]; 
        END GENERATE;
    
    END GENERATE;
    
    
    readdatareal[]                      = rambank.fftreaddatareal[];
    readdataimag[]                      = rambank.fftreaddataimag[];
                                        
    readreal[]                          = rambank.readreal[];
    readimag[]                          = rambank.readimag[];
    
    --*******************
    --*** twiddle rom ***
    --*******************
    
    twidrom.sysclk                      = sysclk;
    twidrom_4k.sysclk                   = sysclk;
    
    -- select twiddle rom implementation, use 4k memory or standard configuration
    if use_4k_mem_blocks == 0 generate
    
        -- show warning if selected 4k memory blocks as they are only present in stratix
        assert (rom_in_4k_mem_blocks == use_4k_mem_blocks) report "cannot implement twiddle rom in 4k memory blocks if stratix parameter is not set" severity warning;
           
        -- standard twiddle rom configuration
           
        twidrom.address[]               = fftcore_a.twidaddress[];
        realtwid[]                      = twidrom.twreal[];
        imagtwid[]                      = twidrom.twimag[];
    
    else generate
    
        -- implement twiddle rom in 4k memory blocks
        -- twiddle memory requires half the amount of storage of standard implementation
        -- stratix architecture only
        assert report "using 4k memory block to implement twiddle rom" severity info;
            
        twidrom_4k.address[]            = fftcore_a.twidaddress[];
        realtwid[]                      = twidrom_4k.twreal[];
        imagtwid[]                  = twidrom_4k.twimag[];
    
    
    end generate;
  
  
END;
